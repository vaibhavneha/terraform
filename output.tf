output "vpc_id" {
  description = "The ID of the VPC"
  value       = aws_vpc.main.id
}


output "subnet_id" {
  value = aws_subnet.sub.id
}

output "gw_id" {
  value = aws_internet_gateway.gw.id
}

output "pub_rt" {
  value = aws_route_table.pub-rt.id
}


