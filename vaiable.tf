variable "vpc_cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "description"
}
variable "instance_tenancy" {
  type        = string
  default     = "default"
  description = "instance tenancy"
}

variable "vpc_name" {
  type        = string
  default     = "ninja-vpc-01"
  description = "vpc name"
}

variable "sn_cidr" {

  description = "Subnet cidr"
  type        = string
  default     = "10.0.1.0/24"
}

variable "sn_name" {
  type    = string
  default = "ninja-pub-sub-01"
}

variable "internet_gateway" {
  type        = string
  default     = "ninja-igw-01"
  description = "description"
}

variable "pub_rt_name" {
  type        = string
  default     = "ninja-route-pub-01"
  description = "description"
}

variable "ami_id" {
  type        = string
  default     = "ami-02f3416038bdb17fb"
  description = "description"
}

variable "instance_type" {
  type        = string
  default     = "t2.micro"
  description = "description"
}

variable "key_name" {
  type        = string
  default     = "girikey"
  description = "description"
}

variable "ec2_name" {
  type        = string
  default     = "airflow-server"
  description = "description"
}

locals {
  ingress_rules = [{
    port        = 22
    description = "For SSH"
    },
    {
      port        = 8080
      description = "For airflow web ui access"
    }]
}

variable "sg_name" {
  type        = string
  default     = "airflow_sg"
  description = "description"
}

